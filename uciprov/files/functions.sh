#!/bin/sh

VERBOSITY=0

blog(){
	echo "[uciprov] $@" >&2
}

blogn(){
	echo -n "[uciprov] $@" >&2
}

bloge(){
	echo "$@" >&2
}

uci(){
	/sbin/uci $@ || echo uci $@ >&2
}

far(){
        local f
        local t
	local sedcmd="sed"
	i=1
	while [ "$i" -lt "$#" ];
	do
		eval f=\$${i}
		i=$(expr $i + 1)
		eval t=\$${i}
		i=$(expr $i + 1)
		sedcmd="$sedcmd -e 's~$f~$t~g'"
	done
        eval $sedcmd
}

#
# function checking if overlay is succesfully mounted
#

check_overlay() {
        mount |grep /overlay | grep -q /dev/
}

#
# blocking function for checking overlay - returns ok when overlay is succesfully mounted
#

wait_for_overlay(){
	while ! check_overlay;
	do
		sleep 3
	done
}

#
# SSL rehash function
#

ssl_rehash() {
	if which openssl >/dev/null && [ -d /etc/ssl/certs ]; then
         (cd /etc/ssl/certs && \
          (find ./ -regex .*\.key -exec chmod 400 {} \;;
           find ./ -type l -regex .*\.0$ -exec bash -c 'rm -f {}' \;;
           find ./ -regex .*\.crt -exec bash -c 'chmod 444 {}; ln -s {} `openssl x509 -noout -hash < {}`.0' \;
           find ./ -regex .*\.pem -exec bash -c 'chmod 444 {}; ln -s {} `openssl x509 -noout -hash < {}`.0' \;
          )
         )
	fi
}

fetch_uri(){
	local tmpdir=/tmp/ucifetch$$
	local uri="$1"
	local dst="$2"
case $1 in
file:*)
	local f=$(echo $uri | cut -d '/' -f 3-)
	cat $f
	;;
ftp:*|http:*|https:*)
	wget -q -O - "$uri"
	;;
tftp:*)
	local h=$(echo $uri | cut -d '/' -f 3)
	local p=$(echo $uri | cut -d '/' -f 4-)
	local f=$(basename $uri)
	mkdir -p $tmpdir && \
	 atftp $h <<EOF 
mode octet
get $p $tmpdir/$f
EOF
        cat $tmpdir/$f && rm -rf $tmpdir
	;;
rsync:*)
	mkdir -p $tmpdir && \
	rsync "$uri" $tmpdir/ && \
	cat $tmpdir/$(basename "$1") && \
	rm -rf $tmpdir
	;;
ssh:*)
	mkdir -p $tmpdir && \
	scp "$uri" $tmpdir/ && \
	cat $tmpdir/$(basename "$1") && \
	rm -rf $tmpdir
	;;
9p:*)
	local h=$(echo $uri | cut -d '/' -f 3)
	local p=$(echo $uri | cut -d '/' -f 4-)
	local f=$(basename $uri)
	mkdir -p $tmpdir && \
	mount -o trans=virtio,version=9p2000.L -t 9p "$h" $tmpdir && \
	cat $tmpdir/$(basename "$1") && \
	umount $tmpdir && \
	rm -rf $tmpdir
	;;
*)
	return 1
	;;
esac
}

test_uri(){
case $1 in
ftp:*|http:*|https:*)
	wget -s "$1" 2>/dev/null
	;;
*)
	md5=$(fetch_uri "$1" | md5sum | cut -d ' ' -f 1)
	[ "$md5" <> "d41d8cd98f00b204e9800998ecf8427e" ]
	;;
esac
}

validate_uri(){
	if [ -z "$1" ]; then blog "Empty uri!"; return 1; fi
	if ! test_uri "$1"; then blog "Unreachable uri $1"; return 1; fi
	return
}

setmacro(){
	local mname=$1
	local mvalue=$2
	local val=$(getmacro $mname)
	if (echo $uci_macros|grep -qvw "$mname"); then
		export uci_macros="$uci_macros $mname"
		eval export ${mname}=\"$mvalue\"
	else
		eval export ${mname}=\"$val $mvalue\"
	fi
	#blog "setmacro: $1=$2"
}

getmacro(){
	local mname=$1
	eval local mvalue=\$${mname}
	echo $mvalue
}

listmacros(){
	for i in $uci_macros; 
	do
		echo $i
	done
}

getmacros(){
	for i in $uci_macros; 
	do
		echo "'{$i}' '$(getmacro $i)'"
	done
}

prepare_uri(){
	local puri="$1"	
	if [ -n "$euri" ];
	then
		setmacro uri "$euri"
		setmacro base_uri "$(dirname $euri)"
	else
		setmacro uri ''
	fi

	echo "$puri" | eval far $(getmacros)
}

# Prepare uri for module
# If $2 is given, force to init module even if prereqs are not met
# Returns uri on stdout or false
uciprov_preparemodule(){
	local module="$1"
	local uri
	local override="$2"
	eval uri=\$${module}_uri

	config_load uciprov
	config_get enabled $module enable
	config_get fdonly $module only_on_fd
	config_get version $module version
	config_get factorydefault global factorydefault

	if  [ -z "$override" ];
	then
	  if [ "$enabled" = 0 ] || [ -z "$uri" ] || ( [ "$factorydefault" = "0" ] && [ "$fdonly" = "1" ] )
	  then
		blog "Not runing $module (enable=$enabled, fd=$factorydefault, configfd=$fdonly, uri=$uri)"
		return 1
	  fi
	fi
	meuri=$(prepare_uri $uri)
	md5=$(fetch_uri $meuri | md5sum - | cut -d ' ' -f 1)
	blog "UCI $module uri: $uri>$meuri, our_version=$version, remote_version=$md5"
	if test_uri $meuri;
	then
		if [ -n "$override" ] || ( validate_uri "$meuri" && [ -z "$version" ] || ! [ "$version" = "$md5" ] )
		then
			echo $meuri
			return
		else
			blog "Bad $module uri or versions same!"
			continue
		fi
	else
		blog "$module uri unreachable!"
	fi
}

uciprov_moduleok(){
	local module="$1"
	local version="$2"
	
	applied="$module($euri)"
	uci set uciprov.$module.version="$version"
	uci commit
}

uciprov_moduleer(){
	local module="$1"
	
	uci set uciprov.$module.version="none"
	uci commit
}

uciprov_protoenabled(){
	if echo " $protocols " | grep -q " $1 ";
	then
		blog "Protocol $1 enabled. Runing hooks."
		return 0
	else
		blog "Protocol $1 disabled in config."
		return 1
	fi
}

get_interface_mac_address(){
	#check if interface exists
	if [ -e /sys/class/net/$1/address ];
	then
		macAddr=$(cat /sys/class/net/$1/address)
		echo -ne $macAddr | sed 's/\:/-/g'
	else
		echo -ne ""
	fi
}

get_interface_ip(){
	#check if interface exists
	if [ -e /sys/class/net/$1 ];
	then
		ip=$(ip addr show $1| grep -o "inet [0-9]*\.[0-9]*\.[0-9]*\.[0-9]*" | grep -o "[0-9]*\.[0-9]*\.[0-9]*\.[0-9]*")
		echo -ne $ip
	else
		echo ""
	fi
}

get_net_ifaces(){
	ifaces=$(ls /sys/class/net/)
	echo -ne $ifaces
}

apply_config(){
	if [ ! -z "$1" ];
	then
		fetch_uri "$1" > /tmp/uci.batch && \
		uci batch </tmp/uci.batch &&
		uci commit
	fi
}

