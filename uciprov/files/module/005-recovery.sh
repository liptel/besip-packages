#!/bin/sh

boot_hook_add uciprov_geturi get_uri_static_recovery
boot_hook_add uciprov_preinit uci_recovery
boot_hook_add uciprov_help uci_recovery_help

get_uri_static_recovery(){
	local fduri
	config_load uciprov
	config_get fduri recovery uri_fd

	setmacro fd_uri "$fduri"
}

uci_recovery(){
	local uri=$(uciprov_preparemodule recovery);
	config_load uciprov
	config_get fd_flag recovery fd_flag
	flag=$(fetch_uri "$uri")
	blog "Recovery FD: uri=$uri,flag=$flag,fd_flag=$fd_flag"
	if [ -n "$uri" ] && ( [ "$flag" = "$fd_flag" ] );
	then
	    yes | jffs2reset && reboot -f
	fi
	recovery_dhcp
}

uci_recovery_help(){
	echo "Uciprov recovery module"
}


recovery_sh(){
	config_load uciprov
	config_get shcmd "recovery" script
	[ -n "$shcmd" ] && $shcmd
}

recovery_dhcp(){
	config_load uciprov
	config_get enabled "recovery" dhcp
	! [ "$enabled" = "1" ] && return
	recurl=$(get_dhcp_var $interface boot_file)
	if echo $recurl | grep -q 'uci.default$';
	then
	    blog "Using defaults uci from $recurl and rebooting!"
	    fetch_uri $recurl | uci import
	    uci commit
	    /sbin/reboot -f
	fi
	
	if echo $recurl | grep -q 'recovery.img$';
	then
	      blog "Using recovery image from $recurl and doing sysupgrade!"
	      fetch_uri $recurl >/tmp/recovery.img
	      sysupgrade -v -n /tmp/recovery.img || blog "Error during sysupgrade!"
	fi
}

uci_recovery(){
	recovery_dhcp
	recovery_sh
}

