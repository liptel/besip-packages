#!/bin/sh

boot_hook_add uciprov_geturi get_uri_static_sysupgrade
boot_hook_add uciprov_preapply uci_sysupgrade
boot_hook_add uciprov_help uci_sysupgrade_help

get_uri_static_sysupgrade(){
	local suuri
	config_load uciprov
	config_get suuri sysupgrade uri_static

	setmacro sysupgrade_uri "$suuri"
}

sysupgrade_err(){
	blog "Error during sysupgrade ($1)"
	uciprov_moduleerr sysupgrade
	return 1
}

# if $1 is given, force to sysupgrade
uci_sysupgrade(){
	local uri=$(uciprov_preparemodule sysupgrade $1);
	if [ -n "$uri" ];
	then
		[ "$factorydefault" = "1" ] && nosave="-n"
		[ -n "$1" ] && nosave="-n"
		
		fetch_uri "$uri" >/tmp/su.img && \
		md5=$(md5sum /tmp/su.img | cut -d ' ' -f 1) && \
		uciprov_moduleok sysupgrade "$md5" && \
		cd / && tar -czf /tmp/su.tgz /etc/config/uciprov && \
		 sysupgrade -v $nosave -f /tmp/su.tgz /tmp/su.img  || blog "Error during sysupgrade!"
	fi
}

uci_sysupgrade_help(){
	echo "Uciprov sysupgrade module"
}


