#!/bin/sh

uciprov_protoenabled static && boot_hook_add uciprov_geturi get_uri_static

get_uri_static(){
	local suri1 suri2 suri3
	config_load uciprov
	config_get suri1 "global" uri_static1
	config_get suri2 "global" uri_static2
	config_get suri3 "global" uri_static3

	setmacro uci_uris "$suri1 $suri2 $suri3"
	setmacro uri_static1 "$suri1"
	setmacro uri_static2 "$suri2"
	setmacro uri_static3 "$suri3"
}

