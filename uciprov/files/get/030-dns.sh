#!/bin/sh

if  which unbound-host >/dev/null;
then
	uciprov_protoenabled dnssec && boot_hook_add uciprov_geturi get_uri_by_dnssec
	uciprov_protoenabled dns && boot_hook_add uciprov_geturi get_uri_by_dns
else
	blog "DNS provisioning disabled, missing unbound-host binary."
fi

get_uri_by_dns(){

	config_load uciprov
	config_get dns_prefix "global" dns_prefix
	[ -z "$dns_prefix" ] && return 

	local dnsc
	local hostbin
	if  which unbound-host >/dev/null;
	then
		hostbin=$(which unbound-host)
		dnsc=unbound
	else
		if  which host >/dev/null;
		then
			hostbin=$(which host)
			dnsc=bind
		fi
	fi

	if [ -x "$hostbin" ];
	then
		if [ -n "$secure" ];
		then
			[ "$dnsc" = "bind" ] && { blog "Cannot use DNSSEC, unbound-host not found!" ; return; }
			! [ -f /etc/unbound/root.key ] && { blog "Cannot use DNSSEC, /etc/unbound/root.key not found!"; return; }
			local year=$(date -Idate | cut -d '-' -f 1)
			[ "$year" -lt "2013"  ] && { blog "Cannot use DNSSEC, Bad date $(date)! Is ntpd runing??"; return; }
			secopts="-v -f /etc/unbound/root.key"
		fi
		local opts=""
			
		if [ -z "$secure" ];
		then
			cmd="$hostbin $secopts $opts -t txt $dns_prefix |grep -v 'not found:'"
		else
			cmd="$hostbin $secopts $opts -t txt $dns_prefix |grep -v 'not found:' |grep '(secure)\$'"
		fi
		blog "Executing $cmd"
		eval $cmd |while read foo foo foo foo txt
		do
			echo setmacro uci_uris $txt
			echo setmacro dns_uri $txt
		done >/tmp/uciprov_dns.sh
		. /tmp/uciprov_dns.sh
		rm -f /tmp/uciprov_dns.sh
	fi
}

get_uri_by_dnssec(){
	secure=1
	get_uri_by_dns
}


