#
# Copyright (C) 2013 Liptel team
#
# This is free software, licensed under the GNU General Public License v2.
#

menu "Configuration"
	depends on PACKAGE_uciprov

	menu "Provisioning URI configuration"
			config UCIPROV_USE_DNS
				bool "DNS"

			config UCIPROV_USE_DNSSEC
				bool "DNSSEC"

			config UCIPROV_USE_DHCP
				bool "DHCP"

			config UCIPROV_USE_LLDP
				depends on BROKEN
				bool "LLDP"

			config UCIPROV_USE_STATIC
				bool "STATIC"
			
		config UCIPROV_URI1
			string "Static provisioning URI 1"
			depends on UCIPROV_USE_STATIC
			default "http://uciprov.{d}/{tgt}/uci{fd}"
		config UCIPROV_URI2
			string "Static provisioning URI 2"
			depends on UCIPROV_USE_STATIC
			default "http://uciprov.{d}/{h}.{d}/uci{fd}"
		config UCIPROV_URI3
			string "Static provisioning URI 3"
			depends on UCIPROV_USE_STATIC
			default "http://uciprov.{d}/{mac}/uci{fd}"
		config UCIPROV_DNS_PREFIX
			string "DNS provisioning prefix"
			depends on UCIPROV_USE_DNS||UCIPROV_USE_DNSSEC
			default "besip"
		
	endmenu

	config UCIPROV_INTERFACE
			string "Provisioning URI interface"
			default "wan"

	config UCIPROV_TGZ
			bool "Tgz deployment module"
			default n

	config UCIPROV_SU
			bool "Sysupgrade module"
			default n
			
	config UCIPROV_RECOVERY
			bool "Recovery module"
			default n
	
	menu "Tgz module configuration"
			depends on UCIPROV_TGZ
			
			config UCIPROV_TGZ_ONLYFD
			bool "Apply only when in factory defaults"
			default y

			config UCIPROV_TGZURI
			string "Static URI for .tgz deployment"
			depends on UCIPROV_TGZ && UCIPROV_USE_STATIC
			default "{base_uri}/files{fd}.tgz"
	endmenu

	menu "Sysupgrade module configuration"
			depends on UCIPROV_SU

			config UCIPROV_SUURI
			string "Static URI for sysupgrade"
			depends on UCIPROV_SU && UCIPROV_USE_STATIC
			default "{base_uri}/image{fd}.bin"

			config UCIPROV_SU_ONLYFD
			bool "Apply only when in factory defaults"
			default y
	endmenu

	menu "Generic configuration"
		config UCIPROV_REBOOT_AFTER_CFG_APPLY
			bool "Reboot after CFG is applied"
			default n

		config UCIPROV_DELAY_BEFORE_REBOOT
			string "Delay before reboot"
			default "180"

		config UCIPROV_AUTOSTART
			bool "Autostart enabled"
			default y

		config UCIPROV_WAITFOROVERLAY
			string "Wait for mounted overlay"
			default 60
			help
			  Wait For overlay [seconds] or 0 to not wait
			  
		config UCIPROV_RETRY
			string "Retry in seconds when configuration not applied"
			default "60"
			help
			  Number of seconds to wait before reply to get uci config or 0 to disable retry
			  
		config UCIPROV_RETRY_FAIL
			string "How many seconds of replying to go to recovery"
			default "180"
			help
			  Number of seconds to repeat retry. After this number of seconds, go to recovery
		
		config UCIPROV_REPEAT
			string "If set to other number than 0, repeat uci process this number of seconds"
			default "0"
			help
			  Number of seconds to repeat uciprov process. If set to zero, no retrying will be done.
			  Can be used to repeatly check uci uri to update device in periodic intervals. 
		
	endmenu
	
	menu "Recovery module configuration"
		depends on UCIPROV_RECOVERY

		config UCIPROV_RECOVERY_FDURI
			string "Recovery uri from which to get flag"
			default "{base_uri}/fd.flag"

		config UCIPROV_RECOVERY_FDFLAG
			string "If factory defaults url contains this string, box will be resetet to factory defaults."
			default "1234"
			
		config UCIPROV_RECOVERY_DHCP
			bool "If everything fails, try to get boot_file uri from DHCP and do actions"
			default y
			help
			  It will check, if boot_file end with "recovery.img". If so, it will do sysupgrade.
			  It will also check if boot_file end with "uci.default". If so, it will import uci and reboots.
			  It will also check if boot_file starts with "shell". If so, it will run shell script.	

		config UCIPROV_RECOVERY_SH
			string "Shell script to run when in recovery mode."
			default ""
   
	endmenu

endmenu

