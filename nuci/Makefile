#
# Copyright (C) 2013 OpenWrt.org
#
# This is free software, licensed under the GNU General Public License v2.
# See /LICENSE for more information.
#

include $(TOPDIR)/rules.mk

PKG_NAME:=nuci
PKG_VERSION:=1.0
PKG_RELEASE:=1
PKG_REV:=ab2ace765ac1b271633ca8c70b4210eb7527cadb

PKG_SOURCE:=$(PKG_NAME)-$(PKG_VERSION).tar.gz
PKG_SOURCE_URL:=https://gitlab.labs.nic.cz/router/nuci.git
PKG_SOURCE_PROTO:=git
PKG_SOURCE_VERSION:=$(PKG_REV)
PKG_SOURCE_SUBDIR:=$(PKG_NAME)
PKG_BUILD_DIR:=$(BUILD_DIR)/$(PKG_NAME)
include $(INCLUDE_DIR)/package.mk

define Package/nuci
  SECTION:=net
  CATEGORY:=Network
  TITLE:=nuci NETCONF
  URL:=https://gitlab.labs.nic.cz/router/nuci
  DEPENDS:= +liblua +libnetconf-cznic +libuci +libuci-lua +libxml2 +zlib
endef

define Package/nuci/description
nuci description
endef

TARGET_CFLAGS += $(FPIC) $(TARGET_CPPFLAGS)

TARGET_LDFLAGS += -llua

define Build/Compile
	$(MAKE) -C $(PKG_BUILD_DIR) \
		$(MAKE_FLAGS) \
		PLUGIN_PATH=/usr/share/nuci/ \
		NO_DOC=1 \
		MAX_LOG_LEVEL=LOG_DEBUG
endef

define Package/nuci/install
	$(INSTALL_DIR) $(1)/usr/bin
	$(INSTALL_BIN) \
		$(PKG_BUILD_DIR)/bin/{nuci,test_runner} \
		$(1)/usr/bin/
	$(INSTALL_DIR) $(1)/etc/config
	$(INSTALL_DATA) \
		$(PKG_BUILD_DIR)/example-config/test \
		$(1)/etc/config/
	$(INSTALL_DIR) $(1)/usr/lib
	$(INSTALL_BIN) \
		$(PKG_BUILD_DIR)/lib/libxmlwrap.so \
		$(1)/usr/lib/
		$(INSTALL_DIR) $(1)/usr/share/nuci/lua_plugins endef
	$(INSTALL_DIR) $(1)/usr/share/nuci/lua_plugins
	$(CP) \
		$(PKG_BUILD_DIR)/src/lua_plugins/* \
		$(1)/usr/share/nuci/lua_plugins/
	$(INSTALL_DIR) $(1)/usr/share/nuci/lua_lib
	$(CP) \
		$(PKG_BUILD_DIR)/src/lua_lib/* \
		$(1)/usr/share/nuci/lua_lib/
endef

$(eval $(call BuildPackage,nuci))
