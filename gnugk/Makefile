#
# Copyright (C) 2013 OpenWrt.org
#
# This is free software, licensed under the GNU General Public License v2.
# See /LICENSE for more information.
#

include $(TOPDIR)/rules.mk

PKG_NAME:=gnugk
PKG_VERSION:=3.3
PKG_SUBVERSION:=4
PKG_RELEASE:=2
PKG_MD5SUM:=ab91810e9e8c53207f65e99c20cb017f

PKG_SOURCE:=$(PKG_NAME)-$(PKG_VERSION)-$(PKG_SUBVERSION).tar.gz
PKG_SOURCE_URL:=@SF/openh323gk
PKG_BUILD_DIR:=$(BUILD_DIR)/$(PKG_NAME)-$(PKG_VERSION)

include $(INCLUDE_DIR)/package.mk

CONFIGURE_VARS += \
	PTLIBDIR=$(BUILD_DIR)/ptlib/ \
	HAS_PTLIB=y \
	OPENH323DIR=$(BUILD_DIR)/h323plus/
CONFIGURE_ARGS += \
	--disable-libssh \
	--disable-netsnmp \
	--disable-pgsql \
	--disable-mysql \
	--disable-firebird \
	--enable-unixodbc \
	--enable-sqlite \
	--enable-radius \
	--disable-h46023 \
	--disable-h46017 \
	--disable-h46018 

ifeq ($(CONFIG_SSP_SUPPORT),)
  SSP_FLAG = -fno-stack-protector
else
  SSP_FLAG = -fstack-protector
endif

MFLAGS := \
	OPENH323MAKEDIR=$(BUILD_DIR)/h323plus/ \
	LD=$(TARGET_CXX) \
	SSP_FLAG=$(SSP_FLAG)

define Package/gnugk
  SECTION:=net
  CATEGORY:=Network
  SUBMENU:=Telephony
  TITLE:=GNU Gatekeeper
  URL:=https://gnugk.org
  DEPENDS:=+ptlib +libh323plus +unixodbc +libradiusclient-ng +libsqlite3 +libstdcpp
endef

define Package/gnugk/description
 The GNU Gatekeeper (GnuGk) is a full featured H.323 gatekeeper,
 available freely under GPL license. It forms the basis for a free IP telephony (VOIP)
 or video conferencing system.
endef

define Build/Compile
	$(MAKE) $(MFLAGS) -C $(PKG_BUILD_DIR) optshared
	$(INSTALL_DIR) $(PKG_INSTALL_DIR)/usr/bin
	$(MAKE) $(MFLAGS) -C $(PKG_BUILD_DIR) install PREFIX=$(PKG_INSTALL_DIR)/usr
endef

define Package/gnugk/install
	$(INSTALL_DIR) $(1)/etc/
	$(CP) $(PKG_BUILD_DIR)/etc/gnugk.ini $(1)/etc/gatekeeper.ini
	$(INSTALL_DIR) $(1)/etc/init.d/
	$(INSTALL_BIN) ./files/gnugk.init $(1)/etc/init.d/gnugk
	$(INSTALL_DIR) $(1)/usr/bin
	$(INSTALL_BIN) 	$(PKG_INSTALL_DIR)/usr/bin/gnugk $(1)/usr/bin
endef

$(eval $(call BuildPackage,gnugk))
