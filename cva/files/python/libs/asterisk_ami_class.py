#!/usr/bin/env python
# -*- coding: utf-8 -*-

import telnetlib as tl
import re

AMI_USERNAME = 'python'
AMI_SECRET   = 'python'
AMI_HOST     = '127.0.0.1'


class AstConnection(object):
    def __init__(
                    self,
                    host   = AMI_HOST, 
                    port   = 5038,
                    user   = AMI_USERNAME,
                    secret = AMI_SECRET
                ):
        self.conn   = None
        self.host   = host
        self.port   = port
        self.user   = user
        self.secret = secret

    def connect(self):
        try:
            if not self.conn:
                self.conn = tl.Telnet(
                                        self.host,
                                        self.port
                                     )

        except:
            raise Exception("Connection to Asterisk AMI failed.")
        return

    def send_action(self, action='login', attributes=None):
        """Action has to be string, attributes dictionary"""
        attributes = attributes or dict(Username=self.user, Secret=self.secret, Events='off') # default action should be login, thus using local instance variables
        if self.conn:
            self.conn.write("Action: %s\r\n" % action)
            if attributes and isinstance(attributes, dict):
                for key, value in attributes.iteritems():
                    self.conn.write("%s: %s\r\n" % (key, value))
            self.conn.write("\r\n")

        
    def read_response(self, check_for=None):
        response = self.conn.read_until("\r\n\r\n")
        retval = False
        if check_for:
            retval = True if re.search(check_for, response) else False
        return retval


#ac = AstConnection()
#ac.connect()
#ac.send_action('login')
#print ac.read_response('Success')
