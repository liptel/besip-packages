#!/usr/bin/env python
# -*- coding: utf-8 -*-
from config.config import Config
from pprint import pprint
import socket
import struct
import json
import time
import logging

#Trida pro komunikaci mezi sondou a serverem pomocí TCP rozhraní
class ZabbixTcp(object):
    active_items = []       # pole do kterho je ulozen seznam aktivnich itemu 
    debug = False           # atribut ktery pokud je nastaven na True, tak zobrazuje vracena data pri vsech pozadavich na standardni vystup
    error = ''              # promena do ktere je ulozen text chyby pokud je pri pozadavku vracena
    
    #def __init__(self):
        #logging.basicConfig(filename='logs/requests.log',level=logging.DEBUG)
    
    #Zaslani sledovanych dat ze sondy na server, Params: nazev itemu, hodnota itemu 
    def send_item(self, key, value):
        now = int(time.time());
        result = self.send(
                {"request":"agent data",
                 "data":[
                    {
                        "host": Config.host_name,
                        "key": str(key),
                        "value": value,
                        "clock": str(now)
                    }
                 ],
                 "clock": str(now)
             })
        #logging.info( time.strftime("%Y-%m-%d %H:%M:%S") + ' send data - '+key+':'+str(value))

    def send_item_to_host(self, key, value, host):
        now = int(time.time());
        result = self.send(
                {"request":"agent data",
                 "data":[
                    {
                        "host": host,
                        "key": str(key),
                        "value": value,
                        "clock": str(now)
                    }
                 ],
                 "clock": str(now)
             })
    #Ziskani seznamu aktivnich itemů ze serveru zabbix, tyto itemy server vyžaduje pro zasílání od sond, ulozeni do pole self.active_items    
    def get_active_items(self):
        result = self.send({"request":"active checks","host": Config.host_name})
        if(result == False):
            pprint(tcp.error)
           # logging.info( time.strftime("%Y-%m-%d %H:%M:%S") +' get active items - '+tcp.error)
        else:
            self.active_items = result['data']
            #logging.info( time.strftime("%Y-%m-%d %H:%M:%S") + ' get active items')
        return result
    
    #Zaslani pozadavku na Zabbix server, univerzalni metoda zasila data spolu s pozadovanou hlavickou skrze tcp port
    def send(self, data):
        DATA =  str(data).replace("'","\"")
        HEADER = '''ZBXD\1%s%s'''
        if(self.debug == True):
            pprint(DATA);
        data_length = len(DATA)
        data_header = struct.pack('i', data_length) + '\0\0\0\0'
        data_to_send = HEADER % (data_header, DATA)
        sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

        sock.connect((Config.host, Config.port))
        sock.send(data_to_send)
        response_header = sock.recv(5)
        
        if not response_header == 'ZBXD\1':
            self.error += 'wrong header returned'
            return False

        response_data_header = sock.recv(8)
        response_data_header = response_data_header[:4] 
        response_len = struct.unpack('i', response_data_header)[0]

        response_raw = sock.recv(response_len)

        sock.close()
        try:
            response = json.loads(response_raw)
        except ValueError:
            self.error += 'Json not returned'
            return False
        
        return response;