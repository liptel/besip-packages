#!/usr/bin/env python
# -*- coding: utf-8 -*-
from config.config import Config
import paramiko
import traceback
import sys

#Trida pro pripojeni ke vzdalenemu pocitaci pomoci SSH
class Ssh(object):
    ssh = object    #objekt udrzujici handler daneho pripojeni 
    
    def __init__(self, host, type):
        self.login(host, type)          # pri iniciaci provede prihlaseni k danepu stroji
    # stazeni souboru pomoci sftp   
    def get_file(self, file, target_location, save_location):   
        sftp = paramiko.SFTPClient.from_transport(self.ssh)
        sftp.get(target_location + file, save_location + file)
    # stazeni textoveho souboru pomoci ssh + cat    
    def download(self, file, target_location, save_location):
        data = self.command('cat ' + target_location + file)
        if data != False:
            for line in data.readlines():
                output = open(save_location + file,'wb')
                output.write(line)
                output.close()
    #spusteni libovolneho ssh prikazu na vzdalenem stroji        
    def command(self, command):
        stdin, stdout, stderr  = self.ssh.exec_command(command)
        err = stderr.read();
        if  err:
            print err
            return False
        return stdout
    # prihlaseni ke vzdalenemu stroji     
    def login(self, host, type):
        try:
            host_config = Config.hosts[host]
        except KeyError:
            print 'Host ' + host + ' not defined'           
            sys.exit() 
        try:
            if type == 'ftp':
                self.ssh = paramiko.Transport((host, 22))
                self.ssh.connect(username = host_config['username'], password = host_config['password'])
            elif type == 'ssh':
                self.ssh = paramiko.SSHClient()
                self.ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy())
                self.ssh.connect(host, username = host_config['username'], password = host_config['password'])
        except Exception:
            print traceback.print_exc()
            sys.exit()
            
