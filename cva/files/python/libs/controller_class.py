#!/usr/bin/env python
# -*- coding: utf-8 -*-
from config.config              import Config
from pprint                     import pprint
from libs.zabbix_tcp_class      import ZabbixTcp
from libs.ssh_class             import Ssh
from libs.zabbix_json_class     import ZabbixJson
from libs.crontab_class         import *
from libs.acl_class             import AsteriskCallList
from libs.asterisk_ami_class    import AstConnection
import uuid
import socket
import re
import os
import wave
import contextlib

#Trida pro spravu a spousteni ostatnich objektu, vyuzivani jejich funkci a jejich vzajemnou interakci
class Controller(object):
    Tcp = object
    Cron = object
    Ssh = object
    Jsn = object
    ip = None
    mac = None
    
    # Vytvoření použivanych objektu
    def __init__(self):
        self.Tcp = ZabbixTcp()
        self.Jsn = ZabbixJson()
        #self.Tcp.get_active_items();
        self.Cron = CronTab(user='ubuser')
        self.ip = self.get_ip()
        self.mac = self.get_mac()

    # priprava prikazu pro spusteni python souboru
    def prepare_cmd(self, file, log_file, item):
        command = '/usr/bin/python '+Config.base_dir+file
        if item != False:
            command += ' -i '+item
        if log_file != False:
            command += ' > '+Config.base_dir+log_file
        return command
    # pridani ulohy do cronu
    def add_cron(self, command, period):
        job  = self.Cron.new(command)
        job.minute().every(period)
        self.Cron.write()
    # pridani ulohy do cronu    
    def add_cron_in_hour(self, command, period):
        job  = self.Cron.new(command)
        job.minute().on(int(period))
        self.Cron.write()
    # odstraneni ulohy z cronu
    def remove_cron(self, command):
        finded = self.find_cron(command)
        for item in finded:
            self.Cron.lines.remove(item)
        self.Cron.write()
    # odstraneni vsech cronu    
    def remove_all_crons(self): 
        for item in self.Cron.lines:
            self.Cron.lines.remove(item)
        self.Cron.write()
    # zobrazeni tabulky s crony    
    def print_cron(self):
        print self.Cron.render()
    # nalezeni daneho cronu podle casti retezce ktery obsahuje    
    def find_cron(self, command):
        return self.Cron.find_command(command)
    # stazeni degradovaneho souboru podle nazvu sondy 1, 2 a timestampu    
    def get_final_file(self, from_probe, to_probe, time): 
        self.Ssh = Ssh('10.0.1.242','ftp')
        filename = from_probe+'-'+to_probe+'-'+time+'-in.wav'
        self.Ssh.get_file(filename, Config.recorded_dir, Config.download_files)
    # ziskani macadresy sondy
    def get_mac(self):
        return str(hex(uuid.getnode()).lstrip("0x").rstrip("L")).zfill(12)
        #mac = commands.getoutput("ifconfig eth0 | grep HWaddr | awk '{ print $5 }'")
        if len(mac)==17:
            return mac.replace(':','')
    # ziskani ip adresy sondy
    def get_ip(self):
        return socket.gethostbyname(socket.gethostname())
    # kontrola jestli existuje sonda s danou mac adresou
    def probe_exist(self):
        return self.Jsn.check_host(self.mac);
    # vytvoreni sondy
    def create_probe(self):
        params = {
            "host": self.mac, #"10.0.0.243",
            "name": self.ip,  #"0A12681AC16",
            "groups":[
            {
               "groupid":4
            }
            ],
            "templates":[
                {
                   "templateid":10087
                }
            ],
            "interfaces":[
                {
                    "type":1,
                    "main":1,
                    "useip":1,
                    "ip":"127.0.0.1",
                    "dns":"",
                    "port":10050
                }
            ]
        } 
        return self.Jsn.add_host(params)
    # generovani sip.conf souboru
    def generate_conf(self):
        hosts = self.Jsn.get_hosts(['host','name'])
        items = ''
        try:
            f = file(Config.template_sipconf,"r")
            try:
                content = f.read()
            finally:
                f.close()
        except IOError:
            return False
        matches = re.findall( r'##FOREACH(.*?)FOREACH##', content, re.DOTALL) #(.*)FOREACH##
        for fc in matches:
            for host in hosts: 
                if host['name'] != self.ip:
                    subcontent = fc.replace('##ip##',host['name'])
                    subcontent = subcontent.replace('##mac##',host['host']).replace('##my_mac##',self.mac)
                    items += subcontent
       
        content = content.replace(fc, items)
        content = content.replace("##FOREACH","")
        content = content.replace("FOREACH##","")
        try:
            f = file(Config.sipconf, "w")
            try:
                f.write(content)
                #os.system('cp '+Config.sipconf+' /etc/aterisk/sip.conf')
            finally:
                f.close()
        except IOError:
           return False
        os.popen('sudo cp '+Config.sipconf+' /etc/asterisk/sip.conf', 'w').write("ubuser123456")
        os.popen('sudo asterisk -rx "core restart now"', 'w').write("ubuser123456")
        return True
    
    
    #nahrani novych souboru ze sondy na server a jejich zalohovani do jineho adresare, vrati seznam souboru ktere byly preneseny
    def rsync(self):
        fileList = os.listdir(Config.recorded_dir)
        bckup_command = 'rsync -a '+Config.recorded_dir+' '+Config.bckup_wav_dir+' > /dev/null'
        os.system(bckup_command)
        command = 'rsync -av --remove-source-files --rsh="ssh -i '+Config.rsa_key_path+'" '+Config.recorded_dir+' root@'+Config.host+':'+Config.server_wav_dir+' > /dev/null'
        os.system(command)
        
        return fileList

    # rozesilani hovoru mezi jednotlivymi sondami        
    def send_calls(self):
        ret                 = {'call_number':0, 'ok':0, 'error':0}
        list_of_hosts       = sorted([host['host'] for host in self.Jsn.get_hosts(['host'])])
        acl                 = AsteriskCallList(list_of_hosts, self.mac)
        local_acl           = acl.acl     # list of tuples [(origin, destination), ....]
        asc                 = AstConnection()
        #wav_length = math.ceil(self.get_wav_length(Config.reference_wav))

        for item in local_acl:
            asc.connect()
            asc.send_action('login')
            if asc.read_response('Success'):
                asc.send_action(
                                action      = 'Originate',
                                attributes  = {
                                                'Channel' : 'Local/%s@local-loop' % item[1],
                                                'Context' : 'to-probe',
                                                'Exten'   : '%s' % item[1],
                                                'Priority': '1',
                                                'Timeout' : '30000'
                                                }
                                )
                ret['call_number'] += 1
                if asc.read_response('Success'):
                    ret['ok'] += 1
                else:
                    ret['error'] += 1
            else:
                print "Can't connect to Asterisk"
                return False
        pprint(ret)
        #time.sleep(wav_length) #Cekani kvuli ulozni souboru z provadeneho hovoru
        return True
    #restartovani asterisk sluzby
    def asterisk_restart(self):
        print "Asterisk should be restarted"
        #os.system('/etc/init.d/asterisk restart')
        
    # Spusteni analyzatoru kvality hovoru na serveru
    def run_analyzation(self, file):
        item = 'pesq-'+ self.get_target_mac(file)
        item_exist = self.Jsn.check_item(item, self.mac) #Pokud neexistuje item u teto sondy na serveru
        if item_exist != True:
            self.Jsn.add_item(item, self.mac)   # Tak ji zde vytvor
            
        self.Ssh = Ssh(Config.host,'ssh') 
        if self.Ssh.command('/usr/bin/python '+Config.server_analyzation_script+' '+ file +' '+self.mac+' ') != False :
            return True
        else:
            return False
    
    def get_target_mac(self, file):
        parts = file.split('-')
        try:
            ret = parts[1]
        except IndexError:
            ret = False
        return ret;
    
    def get_wav_length(self, file):
        with contextlib.closing(wave.open(file,'r')) as f:
            frames=f.getnframes()
            rate=f.getframerate()
            duration=frames/float(rate)
        return duration

    def create_edges(self):
        hosts = self.Jsn.get_hosts(['host'])
        for item in hosts:
            for i in hosts:
                if item['host'] != i['host']:
                    item_exist = self.Jsn.check_item('pesq-'+item['host'], i['host']) #Pokud neexistuje item u teto sondy na serveru
                    if item_exist != True:
                        self.Jsn.add_item( 'pesq-'+item['host'],i['host'])
                        