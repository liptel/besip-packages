import requests
from pprint import pprint
import json
 
ZABIX_ROOT = 'http://10.0.1.240/zabbix'
url = ZABIX_ROOT + '/api_jsonrpc.php'
user = "Admin"
password = "zabbix"

########################################
# user.login
########################################

payload = {
    "jsonrpc" : "2.0",
    "method" : "user.login",
    "params": {
      'user': user,
      'password':password,
    },
    "auth" : None,
    "id" : 0,
}
headers = {
    'content-type': 'application/json',
}
res  = requests.post(url, data=json.dumps(payload), headers=headers)
res = res.json()
print 'user.login response'
pprint(res)

payload = {
    "jsonrpc" : "2.0",
    "method" : "host.get",
    "params": {
      'output': [
          'hostid',
          'name'],
    },
    "auth" : res['result'],
    "id" : 2,
}
res2 = requests.post(url, data=json.dumps(payload), headers=headers)
res2 = res2.json()
print 'host.get response'
pprint(res2)