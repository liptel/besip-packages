from pyzabbix import ZabbixAPI
from requests import Session
from pprint import pprint

host = "http://10.0.1.240/zabbix"
s = Session()
s.auth = ("Admin", "zabbix")

zapi = ZabbixAPI(host, s)
zapi.login("Admin", "zabbix")

host_name = 'Zabbix server'

hosts = zapi.host.get(filter={"host": host_name})
if hosts:
    host_id = hosts[0]["hostid"]
    print("Found host id {0}".format(host_id))

    items = zapi.item.get(
    active=1,
    output='extend',
    expandDescription=1,
    expandData='host',
    type=7
    )
    
    for t in items:
        print ('\n')
        pprint(t)
else:
    print("No hosts found")


raw_input();