#!/usr/bin/env python
# -*- coding: utf-8 -*-
import socket
import sys
from json import JSONEncoder

data = {'command':'reload_items','items':'xxx'}
jdata = JSONEncoder().encode(data);
TCP_IP = 'localhost'
TCP_PORT = 10008
BUFFER_SIZE = 1024  # Normally 1024, but we want fast response

try:
    sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    server_address = (TCP_IP, TCP_PORT)
    print >>sys.stderr, 'connecting to %s port %s' % server_address
    sock.connect(server_address)
except socket.error: 
    print >>sys.stderr, 'target not listening'
    sys.exit()

try:
    # Send data
    print >>sys.stderr, 'sending "%s"' % str(jdata)
    sock.sendall(jdata)

    # Look for the response
    amount_received = 0
    amount_expected = len(jdata)
    
    while amount_received < amount_expected:
        data = sock.recv(BUFFER_SIZE)
        amount_received += len(data)
        print >>sys.stderr, 'data sended OK'

finally:
    print >>sys.stderr, 'closing socket'
    sock.close()