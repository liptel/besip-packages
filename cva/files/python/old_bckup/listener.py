import socket
import sys
import logging
from json import JSONDecoder
from libs.controller_class import Controller
from pprint import pprint

control = Controller()

TCP_IP = '10.0.1.241' #'localhost'
TCP_PORT = 10100
BUFFER_SIZE = 1024  # Normally 1024, but we want fast response
 
sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
server_address = (TCP_IP, TCP_PORT)
#print >>sys.stderr, 'starting up on %s port %s' % server_address
sock.bind(server_address)

sock.listen(1)

while True:
    # Wait for a connection
    #print >>sys.stderr, 'waiting for a connection'
    connection, client_address = sock.accept()

    try:
        #print >>sys.stderr, 'connection from', client_address


        while True:
            data = connection.recv(BUFFER_SIZE)
            print >>sys.stderr, 'received "%s"' % data
            
            if data:
                try:
                    jdata = JSONDecoder().decode(data)
                    #pprint(jdata)
                    #print hasattr(jdata, 'command')
                    #if hasattr(jdata, 'command'):
                    try:
                        control.server_command(jdata['command']);
                    except KeyError:
                        print >>sys.stderr, 'request not containt "command"'

                    
                except ValueError:
                    print >>sys.stderr, 'Json not received'
                    break
                finally:
                    connection.sendall(data)
            else:
                break
            
    finally:
        # Clean up the connection
        connection.close()