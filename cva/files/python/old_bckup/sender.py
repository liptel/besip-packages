#!/usr/bin/env python
# -*- coding: utf-8 -*-
from libs.zabbix_tcp_class import ZabbixTcp
import sys
import getopt
import time
from random import randint

def get_params(params):
   argv = sys.argv[1:]
   result = {}
   try:
      opts, args = getopt.getopt(argv,"hi:o:",["ifile=","ofile="])
   except getopt.GetoptError:
      print 'error'
      sys.exit(2)
   for opt, arg in opts:
      if params.has_key(opt):
          result[params[opt]] = arg
   return  result


Tcp = ZabbixTcp()
params = get_params({'-i':'item'})



#TODO calculate selected parameter (item) by pesq algoritmus
#print time.strftime("%Y-%m-%d %H:%M:%S")+ '  Calculate item '+params['item']+' \n';
value = randint(1,100)

#Send calculated value to server
Tcp.send_item(params['item'], value)

#print time.strftime("%Y-%m-%d %H:%M:%S")+ '  Sedn item '+params['item']+' to zabbix server';