#!/usr/bin/env python
# -*- coding: utf-8 -*-
from config.config import Config
from libs.controller_class import Controller
from pprint import pprint
import subprocess


Con = Controller()
#Check if probe exist, if not register it
if Con.probe_exist() == False:
    Con.create_probe();
    
#Generate sip config
Con.generate_conf()

Con.remove_all_crons()

#Con.add_cron_in_hour('/usr/bin/python '+Config.base_dir+'test.py > '+Config.base_dir+'test.txt', 55)

Con.add_cron_in_hour('/usr/bin/python '+Config.base_dir+'call_maker.py > '+Config.base_dir+'logs/calls.log', 02)

Con.add_cron_in_hour('/usr/bin/python '+Config.base_dir+'analyzer.py > '+Config.base_dir+'logs/analyze.log', 32)

Con.print_cron()

#Get list of active items from zabbix server
#Con.Tcp.get_active_items();

#jsn = ZabbixJson()
#jsn.get_active_items()
#pprint(jsn.get_hosts(['name','host','hostid']))


#Remove old sender commands from cron table
#Con.remove_cron(Con.prepare_cmd(Config.sender_file , False, False))

#Con.remove_all_crons()

#Add new commands to cron table
#for item in Con.Tcp.active_items:
    #if hasatr(Con.Tcp.active_items[item], 'delay'):
    #Con.add_cron(Con.prepare_cmd(Config.sender_file , 'log_'+str(item['key'])+'.txt', str(item['key'])), int(item['delay']))

#Con.print_cron()

#subprocess.Popen("python listener.py > /dev/null 2>&1", shell=True)

#key = raw_input('\n press key to end...\n');


