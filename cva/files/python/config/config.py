class Config(object):
    host = '10.0.1.240'     # Ip adresa zabbix serveru
    port = 10051            # Port pro komunikaci se zabbix rozhranim
    host_name = '10.0.1.249'    # ip adresa sondy
    sender_file = 'sender.py'   # script pro rozesilani hovoru
    base_dir = '/home/ubuser/projekt-vqm-sondy/python/'   #zakladni adresar ve kterem jsou umisteny skripty sondy
    recorded_dir = '/var/spool/asterisk/monitor/'   #adresar do ktereho jsou ukladany nahrane degradovane wav soubory
    bckup_wav_dir = '/home/ubuser/projekt-vqm-sondy/python/archive/'    # adresar pro zalohovani wav soubor predtim nez jsou preneseny na server
    download_files = '/home/ubuser/python/wavs/'    #adresar pro stazeni souboru ze sondy na kterou byl provaden hovor, momentalne nepouzivany
    template_sipconf = '/home/ubuser/projekt-vqm-sondy/python/config/sip_temp.conf' #soubor sablony ze ktere je generovan sip.conf soubor
    sipconf = '/home/ubuser/projekt-vqm-sondy/python/config/sip.conf'   #soubor do ktereho je ulozen vysledek generovani sip.conf
    server_wav_dir = '/home/python/data_wavs'           #adresar na serveru do ktereho jsou vkladany nahrane soubory pro analyzu
    server_analyzation_script = '/home/python/add_task.py'  #skript pro pridani souboru do fronty
    rsa_key_path = '/home/ubuser/projekt-vqm-sondy/ssh/sonda1_rsa'  #cesta k privatnimu klici pro pripojeni ssh na serveru
    hosts = {       # definovane prihlasovaci udaje pro pripojeni k sondam - nahrazeno privatnimi klici
        '10.0.1.242':{
            'username':'ubuser',
            'password':'ubuser123456'
        },
        '10.0.1.240':{
            'username':'root',
            'password':'zabbix'
        }
    }
    zabbix_login = 'Admin'  #zabbix server prihlasovaci jmeno    
    zabbix_password = 'zabbix'  #zabbix server heslo
    zabbix_url = 'http://10.0.1.240/zabbix' #zabbix server frontend a rozhrani pro komunikaci
    reference_wav = '/home/ubuser/test_ulaw_25s.wav'
