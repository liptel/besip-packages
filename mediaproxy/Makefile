#
# Copyright (C) 2014 OpenWrt.org
#
# This is free software, licensed under the GNU General Public License v2.
# See /LICENSE for more information.
#

include $(TOPDIR)/rules.mk

PKG_NAME:=mediaproxy
PKG_VERSION:=2.6.0
PKG_RELEASE:=1
PKG_MD5SUM:=726610cf1ee4a3a9855b8cd6adf85502

PKG_SOURCE:=$(PKG_NAME)-$(PKG_VERSION).tar.gz
PKG_SOURCE_URL:=http://download.ag-projects.com/MediaProxy/
PKG_BUILD_DIR:=$(BUILD_DIR)/$(PKG_NAME)-$(PKG_VERSION)

include $(INCLUDE_DIR)/package.mk
$(call include_mk, python-package.mk)

define Package/mediaproxy-common
  SUBMENU:=Telephony
  SECTION:=net
  CATEGORY:=Network
  TITLE:=mediaproxy-common
  URL:=http://mediaproxy.ag-projects.com/
  DEPENDS:=+python +python-application +python-cjson +python-gnutls \
	+python-thor +zope-interface +twisted +twisted-names \
	+libnetfilter-conntrack +libip4tc
endef

define Package/mediaproxy-dispatcher
  SUBMENU:=Telephony
  SECTION:=net
  CATEGORY:=Network
  TITLE:=mediaproxy-dispatcher
  URL:=http://mediaproxy.ag-projects.com/
  DEPENDS:=+mediaproxy-common
endef

define Package/mediaproxy-relay
  SUBMENU:=Telephony
  SECTION:=net
  CATEGORY:=Network
  TITLE:=mediaproxy-relay
  URL:=http://mediaproxy.ag-projects.com/
  DEPENDS:=+mediaproxy-common           
endef

define Package/mediaproxy/description
MediaProxy is a media relay for RTP/RTCP and UDP streams that works in tandem with OpenSIPS 
to provide NAT traversal capability for media streams from SIP user agents located behind NAT.
MediaProxy supports ICE negotiation by behaving like a TURN relay candidate and the policy can 
be controlled from OpenSIPS configuration.
endef

define Package/mediaproxy-common/description
$(call Package/mediaproxy/description)
This package includes files common to all MediaProxy packages.
endef

define Package/mediaproxy-dispatcher/description
$(call Package/mediaproxy/description)
This package provides the Mediaproxy dispatcher.
endef

define Package/mediaproxy-relay/description
$(call Package/mediaproxy/description)
This package provides the Mediaproxy relay.
endef

define Build/Compile
	$(call Build/Compile/PyMod,,build)
	$(call Build/Compile/PyMod,,install --prefix=/usr --root=$(PKG_INSTALL_DIR))
endef

define Package/mediaproxy-common/install
	$(INSTALL_DIR) $(1)/etc/mediaproxy
	$(CP) \
		$(PKG_BUILD_DIR)/config.ini.sample \
		$(1)/etc/mediaproxy/config.ini
	$(INSTALL_DIR) $(1)/etc/mediaproxy/tls
	$(INSTALL_DIR) $(1)/etc/mediaproxy/radius
	$(CP) \
		$(PKG_BUILD_DIR)/radius/dictionary \
		$(1)/etc/mediaproxy/dictionary
	$(INSTALL_DIR) $(1)$(PYTHON_PKG_DIR)/
	$(CP) \
		$(PKG_INSTALL_DIR)$(PYTHON_PKG_DIR)/* \
		$(1)$(PYTHON_PKG_DIR)/
endef

define Package/mediaproxy-dispatcher/install
	$(INSTALL_DIR) $(1)/etc/init.d
	$(INSTALL_BIN) \
		./files/mediaproxy-dispatcher.init \
		$(1)/etc/init.d/mediaproxy-dispatcher
	$(INSTALL_DIR) $(1)/usr/bin
	$(INSTALL_BIN) \
		$(PKG_INSTALL_DIR)/usr/bin/media-dispatcher \
		$(1)/usr/bin/
endef

define Package/mediaproxy-relay/install
	$(INSTALL_DIR) $(1)/etc/init.d
	$(INSTALL_BIN) \
		./files/mediaproxy-relay.init \
		$(1)/etc/init.d/mediaproxy-relay
	$(INSTALL_DIR) $(1)/usr/bin
	$(INSTALL_BIN) \
		$(PKG_INSTALL_DIR)/usr/bin/media-relay \
		$(1)/usr/bin/
endef

$(eval $(call BuildPackage,mediaproxy-common))
$(eval $(call BuildPackage,mediaproxy-dispatcher))
$(eval $(call BuildPackage,mediaproxy-relay))
